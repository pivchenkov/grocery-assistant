"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))

    if n > 30 or n < 0:
        print("Столько нет")
    elif n == 0 or 5 <= n <= 20 or n >= 25:
        print("Пожалуйста,", n, "яблок")
    elif n == 1 or n == 21:
        print("Пожалуйста,", n, "яблоко")
    else:
        print("Пожалуйста,", n, "яблока")


if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant", "yellow"))  # color this caption
    main()
